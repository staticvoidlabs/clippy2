package main

import (
	"fmt"
	"time"

	"github.com/atotto/clipboard"
	"gitlab.com/staticvoidlabs/clippy2/manager"
	"gitlab.com/staticvoidlabs/clippy2/models"

	_ "image/jpeg"
	_ "image/png"
)

var mShouldExit = false
var mCurrentConfig models.Config
var mLastAction string = ""

func main() {

	// Init config.
	mCurrentConfig = manager.GetCurrentConfig()

	fmt.Println(" Clippy (Version " + manager.GetVersionInfo() + ")")
	fmt.Println("")

	// Enter the main loop.
	for !mShouldExit {

		// 01 Update UI.
		manager.UpdateConsoleOutput()

		// 02 Run Youtv post-processing if enabled.
		mCurrentConfig.YoutvPostProcessingEnabled = true
		if mCurrentConfig.YoutvPostProcessingEnabled {
			manager.RunYoutvPostProcessing(mCurrentConfig)
		}

		// 03 Process clipboard content.
		tmpClipboardContent, err := clipboard.ReadAll()

		if err != nil && err.Error() != "Der Vorgang wurde erfolgreich beendet." {
			fmt.Println(err)
		}

		if mLastAction != tmpClipboardContent {

			mLastAction = tmpClipboardContent

			// Get first 23 chars of current clipboard content.
			tmpRunes := []rune(tmpClipboardContent)
			tmpSubstringStart := string(tmpRunes[0:23])

			if tmpSubstringStart != "https://www.youtube.com" {
				//mLastAction = "skipping: " + tmpClipboardContent
				continue
			}

			// Reset clipboard.
			//clipboard.WriteAll("deleted by clippy")

			go manager.BuildDownloadJob(tmpClipboardContent, mCurrentConfig)

		}

		time.Sleep(1 * time.Second)
	}

}
