package manager

import (
	"fmt"
	"os"

	"github.com/inancgumus/screen"
	"gitlab.com/staticvoidlabs/clippy2/models"
)

const colorReset = "\033[0m"
const colorRed = "\033[31m"
const colorGreen = "\033[32m"
const colorYellow = "\033[33m"
const colorBlue = "\033[34m"
const colorPurple = "\033[35m"
const colorCyan = "\033[36m"
const colorWhite = "\033[97m"
const colorGray = "\033[37m"

var mStateHasChanged bool

// UpdateConsoleOutput updates the console output if job state in queue has changed.
func UpdateConsoleOutput() {

	mStateHasChanged = false

	for _, tmpJob := range mDownloadQueue {

		if tmpJob.StateHasChanged {
			mStateHasChanged = true
			break
		}

	}

	if mStateHasChanged {

		clearConsoleOutput()

		fmt.Println(" Clippy (Version " + GetVersionInfo() + ")")
		fmt.Println("")

		for index, tmpJob := range mDownloadQueue {

			if tmpJob.State == "DOWNLOADING" {

				// Set file extension if needed.
				if !mDownloadQueue[index].FileExtSet {
					setFileExtension(&mDownloadQueue[index])
				}

				// Get current file size.
				if mDownloadQueue[index].FileExtSet {
					mDownloadQueue[index].FileSize = getFileSize(tmpJob.FilePath)
				}

				// Print state for current file.
				fmt.Println(string(colorYellow), "   "+tmpJob.State, string(colorReset)+" "+tmpJob.VideoTitle)

			} else if tmpJob.State == "FAILED" {
				fmt.Println(string(colorRed), "   "+tmpJob.State, string(colorReset)+"      "+tmpJob.VideoTitle)
			}

			mDownloadQueue[index].StateHasChanged = false
		}

		fmt.Println("")

		for _, tmpFinishedJob := range mDownloadHistory {

			if tmpFinishedJob.State == "FINISHED" {
				fmt.Println(string(colorGreen), "   "+tmpFinishedJob.State, string(colorReset)+"    "+tmpFinishedJob.VideoTitle)
			}

		}

	}

}

func clearConsoleOutput() {
	screen.Clear()
	screen.MoveTopLeft()
}

func getFileSize(file string) string {

	tmpSize := "n/a"

	fi, err := os.Stat(file)
	if err != nil {
		fmt.Println(err)
	}
	// get the size
	size := fi.Size()
	fmt.Println(size)

	return tmpSize
}

func setFileExtension(job *models.DownloadJob) {

	tmpStringMkv := job.FilePath + ".mkv"
	tmpStringMp4 := job.FilePath + ".mp4"
	tmpStringWebm := job.FilePath + ".webm"
	tmpStringavi := job.FilePath + ".avi"

	if fileExists(tmpStringMkv) {
		job.FilePath = tmpStringMkv
		job.FileExtSet = true
	} else if fileExists(tmpStringMp4) {
		job.FilePath = tmpStringMp4
		job.FileExtSet = true
	} else if fileExists(tmpStringWebm) {
		job.FilePath = tmpStringWebm
		job.FileExtSet = true
	} else if fileExists(tmpStringavi) {
		job.FilePath = tmpStringavi
		job.FileExtSet = true
	}

}
