module gitlab.com/staticvoidlabs/clippy2

go 1.16

require (
	github.com/atotto/clipboard v0.1.2
	github.com/getlantern/systray v1.1.0 // indirect
	github.com/go-toast/toast v0.0.0-20190211030409-01e6764cf0a4
	github.com/gorilla/mux v1.8.0
	github.com/inancgumus/screen v0.0.0-20190314163918-06e984b86ed3 // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	google.golang.org/api v0.40.0

)
