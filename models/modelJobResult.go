package models

// JobResult struct defines the data model to hold the download job channel object.
type JobResult struct {
	VideoID  string
	JobState string
}
