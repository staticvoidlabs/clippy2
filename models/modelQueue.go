package models

// Queue struct defines the data model to hold a download job object.
type Queue struct {
	Jobs []DownloadJob
}
