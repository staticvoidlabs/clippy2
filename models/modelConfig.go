package models

// Config struct defines the data model to hold the current configuration given in "config.json".
type Config struct {
	Version                    string `json:"version"`
	DebugLevel                 string `json:"debugLevel"`
	PathDownloads              string `json:"pathDownloads"`
	YtDlLoggingEnabled         bool   `json:"ytDlLoggingEnabled"`
	FullPathYtDl               string `json:"fullPathYtDl"`
	ArgOutput                  string `json:"argOutput"`
	ArgTemplateName            string `json:"argTemplateName"`
	ArgFormat                  string `json:"argFormat"`
	ArgFormatSelection         string `json:"argFormatSelection"`
	ThumbailCacheDir           string `json:"thumbailCacheDir"`
	YoutubeAPIKey              string `json:"ytApiKey"`
	YoutvPostProcessingEnabled bool   `json:"youtvPostProcessingEnabled"`
	RemoteClippyEnabled        bool   `json:"remoteClippyEnabled"`
	RemoteClippyBaseURL        string `json:"remoteClippyBaseURL"`
}
